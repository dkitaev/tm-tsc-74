package ru.tsc.kitaev.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.api.service.model.IService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIdException;
import ru.tsc.kitaev.tm.model.AbstractModel;
import ru.tsc.kitaev.tm.repository.model.AbstractRepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public abstract class AbstractService<E extends AbstractModel> implements IService<E> {

    @NotNull
    @Autowired
    private AbstractRepository<E> repository;

    @Override
    @NotNull
    @Transactional
    public E add(@NotNull final E entity) {
        return repository.save(entity);
    }

    @Override
    @NotNull
    @Transactional
    public E update(@NotNull final E entity) {
        return repository.save(entity);
    }

    @Override
    @Nullable
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    @Nullable
    public E findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Optional<E> result = repository.findById(id);
        return result.orElse(null);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public int getSize() {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void delete(@NotNull final E entity) {
        repository.delete(entity);
    }

    @Override
    @Transactional
    public void deleteById(@NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        repository.deleteById(id);
    }

}
