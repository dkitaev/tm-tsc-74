package ru.tsc.kitaev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.kitaev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tasks")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TaskDTO extends AbstractOwnerEntityDTO {

    public TaskDTO(@NotNull String name) {
        this.name = name;
    }

    public TaskDTO(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Column
    private String name;

    @NotNull
    @Column
    private String description;

    @NotNull
    @Column
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "startdate")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate = new Date();

    @Nullable
    @Column(name = "finishdate")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date finishDate;

    @Nullable
    @Column(name = "project_id")
    private String projectId = null;

}
