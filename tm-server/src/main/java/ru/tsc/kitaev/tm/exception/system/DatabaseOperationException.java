package ru.tsc.kitaev.tm.exception.system;

import ru.tsc.kitaev.tm.exception.AbstractException;

public final class DatabaseOperationException extends AbstractException {

    public DatabaseOperationException() {
        super("Database operation exception!");
    }

}
