package ru.tsc.kitaev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends AbstractOwnerRepository<Task> {

    @NotNull
    Task findByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
