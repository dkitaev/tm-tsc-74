package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getJdbcUser();

    @NotNull
    String getJdbcPassword();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getJdbcSqlDialect();

    @NotNull
    String getJdbcNbm2ddlAuto();

    @NotNull
    String getJdbcShowSql();

    @NotNull
    String getHibernateCacheUseSecondLevelCache();

    @NotNull
    String getHibernateCacheProviderConfigurationFileResourcePath();

    @NotNull
    String getHibernateCacheRegionFactoryClass();

    @NotNull
    String getHibernateCacheUseQueryCache();

    @NotNull
    String getHibernateCacheUseMinimalPuts();

    @NotNull
    String getHibernateCacheRegionPrefix();

}
