package ru.tsc.kitaev.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.model.ITaskService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIdException;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.repository.model.TaskRepository;

import java.util.List;

@Service
@NoArgsConstructor
public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @Override
    @Nullable
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

}
