package ru.tsc.kitaev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.dto.UserDTO;

@Repository
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

}
