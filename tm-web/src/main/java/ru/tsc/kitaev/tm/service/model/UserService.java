package ru.tsc.kitaev.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.model.IUserService;
import ru.tsc.kitaev.tm.exception.empty.EmptyLoginException;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.repository.model.UserRepository;

@Service
@NoArgsConstructor
public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private UserRepository repository;

    @Nullable
    @Override
    public User findByLogin(@NotNull String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        return repository.findByLogin(login);
    }

}
