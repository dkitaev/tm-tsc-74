package ru.tsc.kitaev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.kitaev.tm.model.AbstractOwnerModel;

import java.util.List;

@NoRepositoryBean
public interface AbstractOwnerRepository<E extends AbstractOwnerModel> extends AbstractRepository<E> {

    void deleteByUserId(@NotNull String userId);

    @NotNull
    List<E> findAllByUserId(@NotNull String userId);

    @NotNull
    E findByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

}
