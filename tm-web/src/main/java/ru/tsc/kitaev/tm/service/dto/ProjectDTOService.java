package ru.tsc.kitaev.tm.service.dto;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kitaev.tm.dto.ProjectDTO;

@Service
@NoArgsConstructor
public class ProjectDTOService extends AbstractOwnerDTOService<ProjectDTO> implements IProjectDTOService {

}
