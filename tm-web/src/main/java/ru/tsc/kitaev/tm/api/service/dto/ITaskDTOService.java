package ru.tsc.kitaev.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskDTOService extends IOwnerDTOService<TaskDTO> {

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId);

}
