package ru.tsc.kitaev.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.kitaev.tm.dto.TaskDTO;
import ru.tsc.kitaev.tm.marker.WebUnitCategory;
import ru.tsc.kitaev.tm.service.dto.TaskDTOService;
import ru.tsc.kitaev.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Category(WebUnitCategory.class)
public class TaskControllerTest {

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1", "Test Task Description 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2", "Test Task Description 2");

    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3", "Test Task Description 3");

    @NotNull
    @Autowired
    private TaskDTOService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext context;

    @NotNull
    private String userId;

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        taskService.add(userId, task1);
        taskService.add(userId, task2);
        taskService.add(userId, task3);
    }

    @After
    public void after() {
        taskService.clear(userId);
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @NotNull final String url = "/task/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final List<TaskDTO> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(4, tasks.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String url = "/task/delete/" + task3.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertNull(taskService.findById(userId, task3.getId()));
        Assert.assertEquals(2, taskService.getSize(userId));
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final String url = "/task/edit/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

}
