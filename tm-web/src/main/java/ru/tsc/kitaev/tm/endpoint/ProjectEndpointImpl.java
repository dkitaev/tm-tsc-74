package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.api.endpoint.ProjectEndpoint;
import ru.tsc.kitaev.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.kitaev.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @Override
    @NotNull
    @WebMethod
    @PostMapping("/add")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectDTO add(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody final ProjectDTO project
    ) {
        return projectService.add(UserUtil.getUserId(), project);
    }

    @Override
    @NotNull
    @WebMethod
    @PutMapping("/save")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectDTO save(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody final ProjectDTO project
    ) {
        return projectService.update(UserUtil.getUserId(), project);
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public List<ProjectDTO> findAll() {
        return projectService.findAll(UserUtil.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    ) {
        return projectService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @Nullable @PathVariable(value = "id") final String id
    ) {
        return projectService.existsById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public int getSize() {
        return projectService.getSize(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void clear() {
        projectService.clear(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody final ProjectDTO project
    ) {
        projectService.delete(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    ) {
        projectService.deleteById(UserUtil.getUserId(), id);
    }

}
