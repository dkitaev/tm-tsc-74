package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.tsc.kitaev.tm.api.endpoint.ISessionEndpoint;
import ru.tsc.kitaev.tm.dto.Fail;
import ru.tsc.kitaev.tm.dto.Result;
import ru.tsc.kitaev.tm.dto.Success;
import ru.tsc.kitaev.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @WebMethod
    @Override
    @NotNull
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        return sessionService.open(login, password);
    }

    @WebMethod
    @Override
    @NotNull
    public Result closeSession(@WebParam(name = "session", partName = "session") @Nullable final SessionDTO session) {
        try {
            sessionService.close(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
