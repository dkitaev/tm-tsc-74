package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    );

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    );

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTaskSorted(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "sort", partName = "sort") @Nullable final String sort
    );

    @Nullable
    @WebMethod
    TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    );

    @NotNull
    @WebMethod
    TaskDTO findTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    );

    @WebMethod
    void removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    );

    @WebMethod
    void removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    );

    @NotNull
    @WebMethod
    TaskDTO createTask(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    );

    @NotNull
    @WebMethod
    TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    );

    @WebMethod
    void removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    );

    @WebMethod
    void updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    );

    @WebMethod
    void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    );

    @WebMethod
    void startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    );

    @WebMethod
    void startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    );

    @WebMethod
    void startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    );

    @WebMethod
    void finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    );

    @WebMethod
    void finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    );

    @WebMethod
    void finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    );

    @WebMethod
    void changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    );

    @WebMethod
    void changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    );

    @WebMethod
    void changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    );

    @WebMethod
    public boolean existsTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    );

    @WebMethod
    public boolean existsTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") final int index
    );

}
