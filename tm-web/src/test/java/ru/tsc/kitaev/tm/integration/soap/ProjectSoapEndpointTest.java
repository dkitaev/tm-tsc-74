package ru.tsc.kitaev.tm.integration.soap;

import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.tsc.kitaev.tm.api.endpoint.AuthEndpoint;
import ru.tsc.kitaev.tm.api.endpoint.ProjectEndpoint;
import ru.tsc.kitaev.tm.client.AuthSoapEndpointClient;
import ru.tsc.kitaev.tm.client.ProjectSoapEndpointClient;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.marker.WebIntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Category(WebIntegrationCategory.class)
public class ProjectSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static AuthEndpoint authEndpoint;

    @NotNull
    private static ProjectEndpoint projectEndpoint;

    @Nullable
    private static String userId;

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1", "Test Project Description 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2", "Test Project Description 2");

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3", "Test Project Description 3");

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("test", "test").getSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        @NotNull Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        userId = authEndpoint.profile().getId();
    }

    @AfterClass
    public static void afterClass() {
        authEndpoint.logout();
    }

    @Before
    public void before() {
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projectEndpoint.add(project1);
        projectEndpoint.add(project2);
    }

    @After
    public void after() {
        projectEndpoint.clear();
    }

    @Test
    public void addTest() {
        projectEndpoint.add(project3);
        @Nullable final ProjectDTO project = projectEndpoint.findById(project3.getId());
        Assert.assertNotNull(project);
    }

    @Test
    public void saveTest() {
        project1.setName("Test Project Update");
        projectEndpoint.save(project1);
        @Nullable final ProjectDTO project = projectEndpoint.findById(project1.getId());
        Assert.assertEquals("Test Project Update", project.getName());
    }

    @Test
    public void findAllTest() {
        @Nullable final List<ProjectDTO> projects = projectEndpoint.findAll();
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final ProjectDTO project = projectEndpoint.findById(project1.getId());
        Assert.assertEquals(project1.getId(), project.getId());
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(projectEndpoint.existsById(project1.getId()));
    }

    @Test
    public void getSizeTest() {
        Assert.assertEquals(2, projectEndpoint.getSize());
    }

    @Test
    public void clearTest() {
        projectEndpoint.clear();
        Assert.assertEquals(0, projectEndpoint.getSize());
    }

    @Test
    public void deleteTest() {
        projectEndpoint.delete(project2);
        Assert.assertNull(projectEndpoint.findById(project2.getId()));
    }

    @Test
    public void deleteByIdTest() {
        projectEndpoint.deleteById(project2.getId());
        Assert.assertEquals(1, projectEndpoint.getSize());
    }

}
