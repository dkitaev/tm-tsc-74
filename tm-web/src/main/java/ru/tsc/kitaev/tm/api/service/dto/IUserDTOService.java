package ru.tsc.kitaev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.dto.UserDTO;

import java.util.List;

public interface IUserDTOService extends IDTOService<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

}
