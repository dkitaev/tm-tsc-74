package ru.tsc.kitaev.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.api.service.model.IOwnerService;
import ru.tsc.kitaev.tm.api.service.model.IUserService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIdException;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.UserNotFoundException;
import ru.tsc.kitaev.tm.model.AbstractOwnerModel;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.repository.model.AbstractOwnerRepository;

import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractOwnerService<E extends AbstractOwnerModel>
        extends AbstractService<E> implements IOwnerService<E> {

    @NotNull
    @Autowired
    private AbstractOwnerRepository<E> repository;

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @NotNull
    @Transactional
    public E add(@Nullable final String userId, @NotNull final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new UserNotFoundException();
        entity.setUser(user);
        return repository.save(entity);
    }

    @Override
    @NotNull
    @Transactional
    public E update(@Nullable final String userId, @NotNull final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (findById(entity.getUser().getId(), entity.getId()) == null) throw new EntityNotFoundException();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new UserNotFoundException();
        entity.setUser(user);
        return repository.save(entity);
    }

    @Override
    @Nullable
    public List<E> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAllByUserId(userId);
    }

    @Override
    @Nullable
    public E findById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        return repository.findByUserIdAndId(userId, id);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return (int) repository.countByUserId(userId);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void delete(@Nullable final String userId, @NotNull final E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (findById(entity.getUser().getId(), entity.getId()) == null) throw new EntityNotFoundException();
        repository.delete(entity);
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        repository.deleteByUserIdAndId(userId, id);
    }

}
