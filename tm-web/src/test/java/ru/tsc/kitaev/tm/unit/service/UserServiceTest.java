package ru.tsc.kitaev.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.enumerated.RoleType;
import ru.tsc.kitaev.tm.marker.WebUnitCategory;
import ru.tsc.kitaev.tm.service.dto.UserDTOService;

import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@Category(WebUnitCategory.class)
public class UserServiceTest {

    @NotNull
    private final UserDTO user1 = new UserDTO();

    @NotNull
    @Autowired
    private UserDTOService userService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired(required = false)
    private PasswordEncoder passwordEncoder;

    private boolean check = true;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("adminTest", "adminTest");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        @NotNull final String passwordHash = passwordEncoder.encode("userTest1");
        user1.setLogin("userTest1");
        user1.setPasswordHash(passwordHash);
        user1.setRole(RoleType.USER);
        userService.add(user1);
    }

    @After
    public void after() {
        if (check)
            userService.deleteById(user1.getId());
    }

    @Test
    public void updateTest() {
        user1.setLogin("userUpdate");
        userService.update(user1);
        Assert.assertEquals("userUpdate", user1.getLogin());
    }

    @Test
    public void findAllTest() {
        @Nullable final List<UserDTO> users = userService.findAll();
        Assert.assertEquals(5, users.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final UserDTO user = userService.findById(user1.getId());
        Assert.assertEquals(user1.getId(), user.getId());
    }

    @Test
    public void findByLoginTest() {
        @Nullable final UserDTO user = userService.findByLogin(user1.getLogin());
        Assert.assertEquals(user1.getLogin(), user.getLogin());
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(userService.existsById(user1.getId()));
    }

    @Test
    public void countTest() {
        Assert.assertEquals(5, userService.getSize());
    }

    @Test
    public void deleteByIdTest() {
        userService.deleteById(user1.getId());
        Assert.assertEquals(4, userService.getSize());
        check = false;
    }

}
