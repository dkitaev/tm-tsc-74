package ru.tsc.kitaev.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.kitaev.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kitaev.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kitaev.tm.dto.CustomUser;

@Controller
public class TasksController {

    @Autowired
    private ITaskDTOService taskService;

    @Autowired
    private IProjectDTOService projectService;

    @GetMapping("/tasks")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskService.findAll(user.getUserId()));
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}
