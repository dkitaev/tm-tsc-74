package ru.tsc.kitaev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.kitaev.tm.dto.AbstractOwnerEntityDTO;

import java.util.List;

@NoRepositoryBean
public interface AbstractOwnerDTORepository<E extends AbstractOwnerEntityDTO> extends AbstractDTORepository<E> {

    void deleteByUserId(@NotNull String userId);

    @NotNull
    List<E> findAllByUserId(@NotNull String userId);

    @Nullable
    E findByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

}
