package ru.tsc.kitaev.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.Result;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.marker.WebIntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(WebIntegrationCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private static final HttpHeaders HEADERS = new HttpHeaders();

    @Nullable
    private static String sessionId;

    @Nullable
    private static String userId;

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1", "Test Project Description 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Project 2", "Test Project Description 2");

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Project 3", "Test Project Description 3");

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        HEADERS.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        HEADERS.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        @NotNull final String urlProfile = "http://localhost:8080/api/auth/profile";
        @NotNull final ResponseEntity<UserDTO> responseProfile = restTemplate.exchange(urlProfile, HttpMethod.GET, new HttpEntity<>(HEADERS), UserDTO.class);
        userId = responseProfile.getBody().getId();
    }

    private static ResponseEntity<List> sendRequestList(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<List> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<ProjectDTO> sendRequest(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<ProjectDTO> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, ProjectDTO.class);
    }

    @AfterClass
    public static void afterClass() {
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(HEADERS));
    }

    @Before
    public void before() {
        @NotNull final String url = BASE_URL + "add/";
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project1, HEADERS));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project2, HEADERS));
    }

    @After
    public void after() {
        @NotNull final String url = BASE_URL + "clear/";
        sendRequestList(url, HttpMethod.DELETE, new HttpEntity<>(HEADERS));
    }

    @Test
    public void addTest() {
        @NotNull final String url = BASE_URL + "add/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project3, HEADERS));
        @NotNull final String findUrl = BASE_URL + "findById/" + project3.getId();
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
    }

    @Test
    public void saveTest() {
        @NotNull final String url = BASE_URL + "save/";
        project1.setName("Test Project Update");
        sendRequest(url, HttpMethod.PUT, new HttpEntity<>(project1, HEADERS));
        @NotNull final String findUrl = BASE_URL + "findById/" + project1.getId();
        Assert.assertEquals("Test Project Update", sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().getName());
    }

    @Test
    public void findAllTest() {
        @NotNull final String url = BASE_URL + "findAll/";
        Assert.assertEquals(2, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String url = BASE_URL + "findById/" + project1.getId();
        Assert.assertNotNull(sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
    }

    @Test
    public void deleteTest() {
        @NotNull final String url = BASE_URL + "delete/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(project1, HEADERS));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(1, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().size());
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String url = BASE_URL + "deleteById/" + project2.getId();
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(project2, HEADERS));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(1, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().size());
    }

    @Test
    public void clearTest() {
        @NotNull final String url = BASE_URL + "clear/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(project1, HEADERS));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().size());
    }

}
