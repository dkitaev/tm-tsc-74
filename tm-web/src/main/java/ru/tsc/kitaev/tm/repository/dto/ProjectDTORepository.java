package ru.tsc.kitaev.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.dto.ProjectDTO;

@Repository
public interface ProjectDTORepository extends AbstractOwnerDTORepository<ProjectDTO> {

}
