package ru.tsc.kitaev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.model.User;

@Repository
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    User findByLogin(@NotNull final String login);

}
