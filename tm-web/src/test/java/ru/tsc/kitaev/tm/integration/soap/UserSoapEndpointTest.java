package ru.tsc.kitaev.tm.integration.soap;

import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.tsc.kitaev.tm.api.endpoint.AuthEndpoint;
import ru.tsc.kitaev.tm.api.endpoint.UserEndpoint;
import ru.tsc.kitaev.tm.client.AuthSoapEndpointClient;
import ru.tsc.kitaev.tm.client.UserSoapEndpointClient;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.enumerated.RoleType;
import ru.tsc.kitaev.tm.marker.WebIntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
@AutoConfigureMockMvc
@Category(WebIntegrationCategory.class)
public class UserSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static AuthEndpoint authEndpoint;

    @NotNull
    private static UserEndpoint userEndpoint;

    @Autowired(required = false)
    private PasswordEncoder passwordEncoder;

    @NotNull
    private final UserDTO user1 = new UserDTO();

    private boolean check = true;

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("adminTest", "adminTest").getSuccess());
        userEndpoint = UserSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider userBindingProvider = (BindingProvider) userEndpoint;
        @NotNull Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        userBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void afterClass() {
        authEndpoint.logout();
    }

    @Before
    public void before() {
        @NotNull final String passwordHash = passwordEncoder.encode("userTest1");
        user1.setLogin("userTest1");
        user1.setPasswordHash(passwordHash);
        user1.setRole(RoleType.USER);
        userEndpoint.add(user1);
    }

    @After
    public void after() {
        if (check)
            userEndpoint.deleteById(user1.getId());
    }

    @Test
    public void saveTest() {
        user1.setLogin("userUpdate");
        userEndpoint.save(user1);
        Assert.assertEquals("userUpdate", user1.getLogin());
    }

    @Test
    public void findAllTest() {
        @Nullable final List<UserDTO> users = userEndpoint.findAll();
        Assert.assertEquals(5, users.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final UserDTO user = userEndpoint.findById(user1.getId());
        Assert.assertEquals(user1.getId(), user.getId());
    }

    @Test
    public void findByLoginTest() {
        @Nullable final UserDTO user = userEndpoint.findByLogin(user1.getLogin());
        Assert.assertEquals(user1.getLogin(), user.getLogin());
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(userEndpoint.existsById(user1.getId()));
    }

    @Test
    public void countTest() {
        Assert.assertEquals(5, userEndpoint.getSize());
    }

    @Test
    public void deleteByIdTest() {
        userEndpoint.deleteById(user1.getId());
        Assert.assertEquals(4, userEndpoint.getSize());
        check = false;
    }

}
