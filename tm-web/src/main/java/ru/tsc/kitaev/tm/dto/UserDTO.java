package ru.tsc.kitaev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.RoleType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "users")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserDTO extends AbstractEntityDTO {

    @NotNull
    @Column
    private String login;

    @NotNull
    @Column(name = "passwordhash")
    private String passwordHash;

    @Nullable
    private RoleType role = RoleType.USER;

}
