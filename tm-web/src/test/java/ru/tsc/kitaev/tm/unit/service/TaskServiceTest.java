package ru.tsc.kitaev.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.TaskDTO;
import ru.tsc.kitaev.tm.marker.WebUnitCategory;
import ru.tsc.kitaev.tm.service.dto.ProjectDTOService;
import ru.tsc.kitaev.tm.service.dto.TaskDTOService;
import ru.tsc.kitaev.tm.util.UserUtil;

import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@Category(WebUnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Project 1", "Test Project Description 1");

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1", "Test Task Description 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2", "Test Task Description 2");

    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3", "Test Task Description 3");

    @NotNull
    @Autowired
    private ProjectDTOService projectService;

    @NotNull
    @Autowired
    private TaskDTOService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private String userId;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        task1.setProjectId(project1.getId());
        task2.setProjectId(project1.getId());
        task3.setProjectId(project1.getId());
        projectService.add(userId, project1);
        taskService.add(userId, task1);
        taskService.add(userId, task2);
        taskService.add(userId, task3);
    }

    @After
    public void after() {
        taskService.clear(userId);
        projectService.delete(project1);
    }

    @Test
    public void addTest() {
        @NotNull final TaskDTO newTest = new TaskDTO("Test Task 4", "Test Task Description 4");
        taskService.add(userId, newTest);
        Assert.assertEquals("Test Task 4", newTest.getName());
    }

    @Test
    public void updateTest() {
        task1.setName("Test Task Update");
        taskService.update(userId, task1);
        Assert.assertEquals("Test Task Update", task1.getName());
    }

    @Test
    public void findAllTest() {
        @Nullable final List<TaskDTO> tasks = taskService.findAll(userId);
        Assert.assertEquals(3, tasks.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final TaskDTO task = taskService.findById(userId, task1.getId());
        Assert.assertEquals(task1.getId(), task.getId());
    }

    @Test
    public void existsByIdTest() {
        Assert.assertFalse(taskService.existsById(userId, ""));
        Assert.assertTrue(taskService.existsById(userId, task1.getId()));
    }

    @Test
    public void getSizeTest() {
        Assert.assertEquals(3, taskService.getSize(userId));
    }

    @Test
    public void clearTest() {
        taskService.clear(userId);
        Assert.assertEquals(0, taskService.getSize(userId));
    }

    @Test
    public void deleteTest() {
        taskService.delete(userId, task3);
        Assert.assertNull(taskService.findById(userId, task3.getId()));
    }

    @Test
    public void deleteByIdTest() {
        taskService.deleteById(userId, task3.getId());
        Assert.assertEquals(2, taskService.getSize(userId));
    }

    @Test
    public void findAllByProjectIdTest() {
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, project1.getId());
        Assert.assertEquals(3, tasks.size());
    }

}
