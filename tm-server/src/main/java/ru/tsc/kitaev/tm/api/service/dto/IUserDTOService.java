package ru.tsc.kitaev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.dto.UserDTO;

public interface IUserDTOService extends IDTOService<UserDTO> {

    boolean existsById(@NotNull final String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    boolean isLoginExists(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    boolean isEmailExists(@Nullable String email);

    void removeByLogin(@Nullable String login);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    void setPassword(@Nullable String userId, @Nullable String password);

    void setPassword(@NotNull UserDTO user, @NotNull String password);

    void updateUser(@Nullable String userId, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
