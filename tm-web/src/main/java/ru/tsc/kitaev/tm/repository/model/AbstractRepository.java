package ru.tsc.kitaev.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.kitaev.tm.model.AbstractModel;

@NoRepositoryBean
public interface AbstractRepository<E extends AbstractModel> extends JpaRepository<E, String> {

}
